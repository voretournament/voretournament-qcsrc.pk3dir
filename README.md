This repository contains game code snapshots of the Vore Tournament mod for Xonotic. They are built against the latest xonotic-data.pk3dir + voretournament-mod repositories and updated periodically.

Important: This snapshot is meant to be used as a last resort for people who can't compile the game code on their own! It's highly recommended that you attempt building the game yourself using gmqcc first. Those snapshots lag behind changes in the Xonotic and / or Vore Tournament repositories... as such they don't always mirror the functionality of the latest source code, while in some cases incompatibilities with other components may arise.

Instructions: After installing Xonotic and the Vore Tournament mod, clone this repository inside your data_voretournament directory.